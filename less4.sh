docker rm -vf $(docker ps -aq)
docker rmi -f $(docker images -aq)
docker pull debian:9
docker build -t lesson4:v1 .
kek=$(docker images | grep lesson4 | awk 'NR==1{print $3}')
docker run -v $(pwd)/nginx.conf:/opt/nginx/conf/nginx.conf -d -p 1234:80 $kek
curl 127.0.0.1:1234
